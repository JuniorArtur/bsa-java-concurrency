package bsa.java.concurrency.hashing;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;

@Service
public class DiagonalHashGenerator {

    @Async
    public CompletableFuture<Long> calculateHash(byte[] image) {
        try {
            final var img = ImageIO.read(new ByteArrayInputStream(image));
            return CompletableFuture.completedFuture(calculateDHash(preprocessImage(img)));
        } catch (IOException err) {
            throw new IllegalArgumentException(err.getMessage());
        }
    }

    private static BufferedImage preprocessImage(BufferedImage image) {
        final var result = image.getScaledInstance(9, 9, Image.SCALE_SMOOTH);
        final var output = new BufferedImage(9, 9, BufferedImage.TYPE_BYTE_GRAY);
        output.getGraphics().drawImage(result, 0, 0, null);
        return output;
    }

    private static int brightnessScore(int rgb) {
        return rgb & 0b11111111;
    }

    public static long calculateDHash(BufferedImage processedImage) {
        long hash = 0;
        for (var row = 1; row < 8; row++) {
            for (var col = 1; col < 9; col++) {
                final var prev = brightnessScore(processedImage.getRGB(col - 1, row - 1));
                final var current = brightnessScore(processedImage.getRGB(col, row));
                hash |= current > prev ? 1 : 0;
                hash <<= 1;
            }
        }
        return hash;
    }

}
