package bsa.java.concurrency.image;

import bsa.java.concurrency.fs.FileSystem;
import bsa.java.concurrency.hashing.DiagonalHashGenerator;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import javassist.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

@Service
public class ImageService {

    private static final Logger logger = LoggerFactory.getLogger(ImageService.class);

    private final ImageRepository imageRepository;

    private final FileSystem imageFileManager;

    private final DiagonalHashGenerator hashGenerator;

    @Autowired
    public ImageService(ImageRepository imageRepository, FileSystem fileSystem,
                        DiagonalHashGenerator hashGenerator) {
        this.imageRepository = imageRepository;
        this.imageFileManager = fileSystem;
        this.hashGenerator = hashGenerator;
    }

    public void save(MultipartFile... files) {
        Arrays.stream(files)
                .parallel()
                .map(file -> {
                    try {
                        return file.getBytes();
                    } catch (IOException ex) {
                        return new byte[0];
                    }
                })
                .forEach(img -> {
                    try {
                        final var imgUrl = imageFileManager.save(img);
                        final var imageEntity = new Image();
                        imageEntity.setUrl(imgUrl.get());
                        imageEntity.setHash(hashGenerator.calculateHash(img).get());
                        imageRepository.save(imageEntity);
                        logger.info("New image with URL: {}, saved", imgUrl);
                    } catch (ExecutionException | InterruptedException | IOException ex) {
                        logger.error(ex.getMessage(), ex);
                    }
                });
    }

    public List<SearchResultDTO> getMatches(MultipartFile file, double threshold) {
        try {
            final var imgHash = hashGenerator.calculateHash(file.getBytes());
            final var matches = imageRepository.searchMatches(imgHash.get(), threshold).get();
            logger.info("Found image matches: {}", matches);
            return matches;
        } catch (InterruptedException | ExecutionException | IOException ex) {
            logger.error(ex.getMessage(), ex);
            return Collections.emptyList();
        }
    }

    public void deleteById(UUID id) throws IOException, NotFoundException {
        if (!imageRepository.existsById(id)) {
            final var notFoundException = new NotFoundException("Image with id: " + id + " not found");
            logger.error(notFoundException.getMessage(), notFoundException);
            throw notFoundException;
        }
        final var image = imageRepository.findById(id).orElseThrow();
        imageFileManager.deleteByUrl(image.getUrl());
        imageRepository.delete(image);
        logger.info("Image with id: {} was deleted", id);
    }

    public void deleteAll() throws IOException {
        imageRepository.deleteAll();
        imageFileManager.deleteAll();
        logger.info("All images were deleted.");
    }

}
