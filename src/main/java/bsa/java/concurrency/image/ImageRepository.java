package bsa.java.concurrency.image;

import bsa.java.concurrency.image.dto.SearchResultDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Repository
public interface ImageRepository extends JpaRepository<Image, UUID> {

    @Async
    @Query(nativeQuery = true,
            value = "select cast(img.id as varchar) as id, " +
                           "img.image_url as image, " +
                           "(1 - ( " +
                                    "select length(replace(cast(x as text), '0', '')) " +
                                    "from (values (cast(:hash as bit(64)) # cast(img.image_hash as bit(64)))) as something(x) " +
                                ") / 64. " +
                           ") as match_percent " +
                    "from images img " +
                    "where (1 - ( " +
                                   "select length(replace(cast(x as text), '0', '')) " +
                                   "from (values (cast(:hash as bit(64)) # cast(img.image_hash as bit(64)))) as something(x) " +
                               ") / 64. " +
                          ") >= :threshold " +
                    "order by match_percent desc "
    )
    CompletableFuture<List<SearchResultDTO>> searchMatches(@Param("hash") long hash, @Param("threshold") double threshold);

}
