package bsa.java.concurrency.image;

import bsa.java.concurrency.image.dto.SearchResultDTO;
import javassist.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/image")
public class ImageController {

    private static final Logger logger = LoggerFactory.getLogger(ImageController.class);

    private final ImageService imageService;

    @Autowired
    public ImageController(ImageService imageService) {
        this.imageService = imageService;
    }

    @PostMapping("/batch")
    @ResponseStatus(HttpStatus.CREATED)
    public void batchUploadImages(@RequestParam("images") MultipartFile... files) {
        this.imageService.save(files);
    }

    @PostMapping("/search")
    public ResponseEntity<List<SearchResultDTO>> searchMatches(@RequestParam("image") MultipartFile file,
                                                               @RequestParam(value = "threshold", defaultValue = "0.9")
                                                                       double threshold) {
        if (threshold > 1) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        final var matches = this.imageService.getMatches(file, threshold);
        if (matches.isEmpty()) {
            this.imageService.save(file);
        }
        return new ResponseEntity<>(matches, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public HttpStatus deleteImage(@PathVariable("id") UUID imageId) {
        try {
            this.imageService.deleteById(imageId);
            return HttpStatus.NO_CONTENT;
        } catch (NotFoundException | IOException ex) {
            logger.error(ex.getMessage(), ex);
            return HttpStatus.NOT_FOUND;
        }
    }

    @DeleteMapping("/purge")
    public HttpStatus purgeImages() {
        try {
            this.imageService.deleteAll();
            return HttpStatus.NO_CONTENT;
        } catch (IOException ex) {
            logger.error(ex.getMessage(), ex);
            return HttpStatus.NOT_FOUND;
        }
    }

}
