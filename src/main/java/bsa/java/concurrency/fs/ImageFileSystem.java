package bsa.java.concurrency.fs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Service
public class ImageFileSystem implements FileSystem {

    @Value("app.images-dir")
    private Path imagesDirPath;

    private final FilePathMapper filePathMapper;

    @Autowired
    public ImageFileSystem(FilePathMapper filePathMapper) {
        this.filePathMapper = filePathMapper;
    }

    @Async
    @Override
    public CompletableFuture<String> save(byte[] bytes) throws IOException {
        if (!Files.exists(imagesDirPath)) {
            Files.createDirectories(imagesDirPath);
        }
        final var generatedImgPath = imagesDirPath.resolve(UUID.randomUUID().toString() + ".jpg");
        Files.createFile(generatedImgPath);
        try (final var out = new FileOutputStream(generatedImgPath.toString())) {
            out.write(bytes);
        }
        return CompletableFuture.completedFuture(filePathMapper.mapPathToStringUrl(generatedImgPath));
    }

    @Async
    @Override
    public void deleteByUrl(String url) throws IOException {
        Files.deleteIfExists(filePathMapper.mapStringUrlToPath(url));
    }

    @Async
    @Override
    public void deleteAll() throws IOException {
        try (final var files = Files.walk(imagesDirPath)) {
            files.sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .filter(File::isFile)
                    .forEach(File::delete);
        }
    }

}
