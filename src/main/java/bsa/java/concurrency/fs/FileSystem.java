package bsa.java.concurrency.fs;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;

public interface FileSystem {

    CompletableFuture<String> save(byte[] bytes) throws IOException;

    void deleteByUrl(String url) throws IOException;

    void deleteAll() throws IOException;

}
