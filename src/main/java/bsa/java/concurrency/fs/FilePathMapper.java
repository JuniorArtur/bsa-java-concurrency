package bsa.java.concurrency.fs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.nio.file.Path;

@Service
public class FilePathMapper {

    @Value("${app.url}")
    private String localhostUrl;

    public String mapPathToStringUrl(Path path) {
        return localhostUrl.concat(path.toString().replace("\\", "/"));
    }

    public Path mapStringUrlToPath(String url) {
        return Path.of(url.replace(localhostUrl, ""));
    }

}
